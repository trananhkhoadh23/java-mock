package com.example.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String questionId;
    @Column(columnDefinition = "longtext")
    private String questionBody;
    private int topic;
    @OneToMany(mappedBy = "question",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Answer> answerList = new ArrayList<>();
    private String refLink = "";

    public Question(String questionId, String questionBody) {
        this.questionId = questionId;
        this.questionBody = questionBody;
    }
}




