package com.example.demo;

import com.example.demo.entity.Answer;
import com.example.demo.entity.Question;
import com.example.demo.repository.QuestionRepository;
import com.example.demo.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Slf4j
public class ProcessingPdfApplication {
    private static QuestionRepository questionRepository;

    public ProcessingPdfApplication(QuestionRepository questionRepository) {
        ProcessingPdfApplication.questionRepository = questionRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(ProcessingPdfApplication.class, args);
        File file = new File("E:\\Works\\Java\\Projects\\PDF\\input.pdf");
        String inputText = readTextFromFile(file);
        extractDataFromText(inputText);
    }

    private static void extractDataFromText(String inputText) {
        String[] questionList = inputText.split(Constants.QUESTION_BREAK);
        List<Question> questions = new ArrayList<>();
        //loop for each question
        for (int i = 1; i < questionList.length; i++) {
            //delete question contains Drag drop or hotspot
            if (!(questionList[i].contains(Constants.DRAG_DROP) ||
                    questionList[i].contains(Constants.HOTSPOT))) {
                String[] temp = questionList[i].split(Constants.ANSWERS_BREAK)[1]
                        .split(Constants.CORRECT_ANSWERS_BREAK);
                if (temp.length <= 0) {
                    log.error("Temp array is empty");
                    throw new NullPointerException();
                }

                Question question = getQuestion(questionList[i], questionList[i-1], temp);
                List<Answer> answers = getAnswers(question, temp);
                question.setAnswerList(answers);
                questions.add(question);
            }
        }
        questionRepository.saveAll(questions);
    }

    private static String readTextFromFile(File file) {
        String outputText = null;
        try (PDDocument document = PDDocument.load(file)) {
            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            outputText = pdfTextStripper.getText(document);
        } catch (IOException e) {
            log.error("Read file failed!");
        }
        return outputText;
    }

    private static Question getQuestion(String inputQuestion, String previousQues, String[] temp) {
        String questionId = inputQuestion.split(Constants.LINE_BREAK)[0];
        String questionBody = inputQuestion.split(Constants.ANSWERS_BREAK)[0]
                .split(Constants.LINE_BREAK,Constants.CORRECT_ANS_LIMIT_BREAK)[1];
        if (questionId == null || questionBody == null) log.error("Question is null!");
        Question q = new Question(questionId, questionBody);
        if (temp[1].contains(Constants.REFERENCES)) {
            String refLink = temp[1].substring(temp[1].indexOf(Constants.REFERENCES)
                            + Constants.CHARACTER_NUMBER_OF_REFERENCES,
                    temp[1].indexOf(Constants.REFERENCES_ICON));
            if (refLink.split(Constants.LINE_BREAK, Constants.REF_LIMIT_BREAK)[Constants.SECOND_LINE_OF_REF]
                    .contains(Constants.SPACE))
                refLink = refLink.split(Constants.LINE_BREAK, Constants.REF_LIMIT_BREAK)[Constants.FIRST_LINE_OF_REF];
            q.setRefLink(refLink);
        }
        if (previousQues.contains(Constants.TOPIC_BREAK)) {
            int topic = Integer.parseInt(previousQues.
                    substring(previousQues.lastIndexOf(Constants.TOPIC_BREAK)
                            + Constants.CHARACTER_NUMBER_OF_TOPIC));
            q.setTopic(topic);
        }
        return q;
    }

    private static List<Answer> getAnswers(Question question, String[] temp) {
        List<Answer> answers = new ArrayList<>();
        String[] allAnswersBody = (Constants.A_WITH_DOT + temp[0]).split(Constants.REGEX_OF_QUESTION_PREFIX);
        String correctAns = temp[1].split(Constants.LINE_BREAK, Constants.CORRECT_ANS_LIMIT_BREAK)[0]
                .replace(Constants.COMMA, Constants.BLANK).trim();
        for (String s : allAnswersBody) {
            Answer a = new Answer();
            a.setAnsBody(s);
            if (correctAns.contains(s.split(Constants.DOT)[0])) {
                a.setTrue(true);
            }
            a.setQuestion(question);
            answers.add(a);
        }
        return answers;
    }
}
