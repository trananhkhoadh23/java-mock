package com.example.demo.utils;

public class Constants {
    public final static String QUESTION_BREAK ="Question #";
    public final static String LINE_BREAK ="\\n";
    public final static String DRAG_DROP ="DRAG DROP";
    public final static String HOTSPOT ="HOTSPOT";
    public final static String ANSWERS_BREAK ="A\\. ";
    public final static String CORRECT_ANSWERS_BREAK ="Correct Answer:";
    public final static String TOPIC_BREAK ="Topic ";
    public final static String REFERENCES ="Reference:";
    public final static String REFERENCES_ICON ="\uF147 \uF007";
    public final static int REF_LIMIT_BREAK = 3 ;
    public final static int CORRECT_ANS_LIMIT_BREAK = 2 ;
    public final static int FIRST_LINE_OF_REF = 1 ;
    public final static int SECOND_LINE_OF_REF = 2 ;
    public final static int CHARACTER_NUMBER_OF_REFERENCES = 10;
    public final static int CHARACTER_NUMBER_OF_TOPIC = 6;
    public final static String SPACE =" ";
    public final static String COMMA =",";
    public final static String BLANK ="";
    public final static String DOT ="\\.";
    public final static String REGEX_OF_QUESTION_PREFIX ="(?=[A-Z]\\.)";
    public final static String A_WITH_DOT ="A. ";

}
